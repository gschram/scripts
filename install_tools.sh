#!/bin/bash

# Geerten Schram

TMPDIR=~/tmp
INSTALLDIR=~/bin
DEBUG=0

if [ ${DEBUG} -eq 1 ]; then set -x; fi

# prereq checken
if [ ! -d "${TMPDIR}" ]; then
  mkdir -p ${TMPDIR}
fi
if [ ! -d "${INSTALLDIR}" ]; then
  mkdir -p ${INSTALLDIR}
fi

# ======= fixed versions ==============
TOOLS_FIX_VERSION=(
  'v1.30.6'  # 0 kubectl
  'latest'   # 1 kustomize
  'latest'   # 2 k9s
  '2.4.0'    # 3 flux2
  'latest'   # 4 mc
  'latest'   # 5 yq
  '1.13.2'   # 6 kyverno
  'latest'   # 7 stern
  'latest'   # 8 promlens
  'latest'   # 9 helm
  'latest'   # 10 kind
  '0.1.2'    # 11 chainsaw
  'latest'   # 12 ghorg (gitlab/github group check out tool)
  'latest'   # 13 rancher (cli)
)


# =========================
# This is the magic
#
TOOLS=(kubectl kustomize k9s flux mc yq_linux_amd64 kyverno stern promlens helm kind chainsaw ghorg rancher)
TOOLS_RELEASE_CMD=(
  'curl -L -s https://dl.k8s.io/release/stable.txt'
  'curl -s https://api.github.com/repos/kubernetes-sigs/kustomize/releases | grep browser_download_url | head -1 | cut -d/ -f9 |  tr -d [:space:]'
  'curl -s https://api.github.com/repos/derailed/k9s/releases/latest | grep browser_download_url | head -1 | cut -d/ -f8 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/fluxcd/flux2/releases/latest | grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/minio/mc/releases/latest | grep tag_name | cut -d\" -f4 | cut -d. -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/mikefarah/yq/releases/latest | grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/kyverno/kyverno/releases/latest | grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/stern/stern/releases/latest | grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/prometheus/promlens/releases/latest | grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/helm/helm/releases/latest | grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/kubernetes-sigs/kind/releases/latest | grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/kyverno/chainsaw/releases/latest | grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/gabrie30/ghorg/releases/latest |  grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
  'curl -s https://api.github.com/repos/rancher/cli/releases/latest |  grep browser_download_url | head -1 | cut -d/ -f8 | cut -dv -f2 | tr -d [:space:]'
)
TOOLS_VERSION_CMD=(
  'eval ${TOOLS[0]} version --client=true  2>/dev/null | grep "Client Version" | cut -d: -f2 | tr -d [:space:]'
  'eval ${TOOLS[1]} version --short 2>/dev/null | cut -d" " -f1 | cut -dv -f2 | tr -d [:space:]'
  'eval ${TOOLS[2]} version | grep Version | cut -d: -f2 | tr -d [:space:] | sed -r "s:\x1B\[[0-9;]*[mK]::g"'
  'eval ${TOOLS[3]} version --client | grep flux | cut -dv -f2 | tr -d [:space:]'
  'eval ${TOOLS[4]} --version | grep version | cut -d" " -f3 | cut -d. -f2 | tr -d [:space:]'
  'eval ${TOOLS[5]} --version | grep "version " | cut -dv -f3 | tr -d [:space:]'
  'eval ${TOOLS[6]} version | grep Version | cut -d: -f2 | tr -d [:space:]'
  'eval ${TOOLS[7]} --version | grep version | cut -d: -f2 | tr -d [:space:]'
  'eval ${TOOLS[8]} --version 2>&1 | grep version | head -1 | cut -d" " -f3 | tr -d [:space:]'
  'eval ${TOOLS[9]} version 2>&1 | grep Version |  cut -d: -f2 | cut -dv -f2 | cut -d\" -f1 | tr -d [:space:]'
  'eval ${TOOLS[10]} version 2>&1 | cut -d" " -f2 | cut -dv -f2 | tr -d [:space:]'
  'eval ${TOOLS[11]} version | grep Version | cut -d: -f2 | tr -d [:space:]'
  'eval ${TOOLS[12]} version 2>&1 | cut -dv -f2  | tr -d [:space:] '
  'eval ${TOOLS[13]} --version 2>&1 | cut -d" " -f3  | cut -dv -f2 | tr -d [:space:] '
)

TOOLS_DOWNLOAD_CMD=(
  'curl -s -L -o ${TMPDIR}/${TOOLS[0]} https://dl.k8s.io/release/${AVAILABLE_TOOL_VERSION[0]}/bin/linux/amd64/kubectl'
  'curl -s -L https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${AVAILABLE_TOOL_VERSION[1]}/kustomize_${AVAILABLE_TOOL_VERSION[1]}_linux_amd64.tar.gz -o - | tar xz -C ${TMPDIR} ${TOOLS[1]}'
  'curl -s -L https://github.com/derailed/k9s/releases/download/${AVAILABLE_TOOL_VERSION[2]}/k9s_Linux_amd64.tar.gz -o - | tar xz -C ${TMPDIR} ${TOOLS[2]}'
  'curl -s -L https://github.com/fluxcd/flux2/releases/download/v${AVAILABLE_TOOL_VERSION[3]}/flux_${AVAILABLE_TOOL_VERSION[3]}_linux_amd64.tar.gz -o - | tar xz -C ${TMPDIR} ${TOOLS[3]}'
  'curl -s -L -o ${TMPDIR}/${TOOLS[4]} https://dl.min.io/client/mc/release/linux-amd64/mc'
  'curl -s -L -o ${TMPDIR}/${TOOLS[5]} https://github.com/mikefarah/yq/releases/download/v${AVAILABLE_TOOL_VERSION[5]}/yq_linux_amd64'
  'curl -s -L https://github.com/kyverno/kyverno/releases/download/v${AVAILABLE_TOOL_VERSION[6]}/kyverno-cli_v${AVAILABLE_TOOL_VERSION[6]}_linux_x86_64.tar.gz | tar xz -C ${TMPDIR} ${TOOLS[6]}'
  'curl -s -L https://github.com/stern/stern/releases/download/v${AVAILABLE_TOOL_VERSION[7]}/stern_${AVAILABLE_TOOL_VERSION[7]}_linux_amd64.tar.gz | tar xz -C ${TMPDIR} ${TOOLS[7]}'
  'curl -s -L https://github.com/prometheus/promlens/releases/download/v${AVAILABLE_TOOL_VERSION[8]}/promlens-${AVAILABLE_TOOL_VERSION[8]}.linux-amd64.tar.gz | tar xz -C ${TMPDIR} promlens-${AVAILABLE_TOOL_VERSION[8]}.linux-amd64/promlens --strip-components=1'
  'curl -s -L https://get.helm.sh/helm-v${AVAILABLE_TOOL_VERSION[9]}-linux-amd64.tar.gz | tar xz -C ${TMPDIR} linux-amd64/helm --strip-components=1'
  'curl -s -L -o ${TMPDIR}/${TOOLS[10]} https://kind.sigs.k8s.io/dl/v${AVAILABLE_TOOL_VERSION[10]}/kind-linux-amd64'
  'curl -s -L https://github.com/kyverno/chainsaw/releases/download/v${AVAILABLE_TOOL_VERSION[11]}/chainsaw_linux_amd64.tar.gz | tar xz -C ${TMPDIR} ${TOOLS[11]}'
  'curl -s -L https://github.com/gabrie30/ghorg/releases/download/v${AVAILABLE_TOOL_VERSION[12]}/ghorg_${AVAILABLE_TOOL_VERSION[12]}_Linux_x86_64.tar.gz | tar xz -C ${TMPDIR} ${TOOLS[12]}'
  'curl -s -L https://github.com/rancher/cli/releases/download/v${AVAILABLE_TOOL_VERSION[13]}/rancher-linux-amd64-v${AVAILABLE_TOOL_VERSION[13]}.tar.gz | tar xz -C ${TMPDIR} --strip-components 2'
)

debug () {
  if [ "${DEBUG}" -eq "1" ]; then
    echo ${DEBUG}
    echo; echo
    echo "Check directories"
    echo "${INSTALLDIR}:"
    ls -l ${INSTALLDIR}
    echo
    echo "${TMPDIR}:"
    ls -l ${TMPDIR}
  fi
}

# =========================

for i in ${!TOOLS[@]}; do
  echo
  echo "--- ${TOOLS[$i]} ----"
  ## Get latest released versionnumber of tool
  AVAILABLE_TOOL_VERSION[$i]=$(eval ${TOOLS_RELEASE_CMD[$i]})
  RETVAL=$?
  if [ ${RETVAL} -eq 0 ]; then
    echo "Retrieval version release of ${TOOLS[$i]} succesfull."
  else
    echo "Retrieval version release of ${TOOLS[$i]} failed."
    exit ${RETVAL}
  fi

  echo "Available version of ${TOOLS[$i]} is ${AVAILABLE_TOOL_VERSION[$i]}"

  ## Get current version of the tool
  if [ -x "$(which ${TOOLS[$i]})" ]; then
    CURRENT_TOOL_VERSION[$i]=$(eval ${TOOLS_VERSION_CMD[$i]})
    echo "Current version of ${TOOLS[$i]} is ${CURRENT_TOOL_VERSION[$i]}"
  else
    echo "The tool ${TOOLS[$i]} is not installed"
    CURRENT_TOOL_VERSION[$i]="v0"
  fi

  ## If a fixed version is given set AVAILABLE_TOOL_VERSION to that version
  if [ "${TOOLS_FIX_VERSION[$i]}" != "latest" ]; then
    AVAILABLE_TOOL_VERSION[$i]="${TOOLS_FIX_VERSION[$i]}"
    echo "Given fixed version for ${TOOLS[$i]} is ${TOOLS_FIX_VERSION[$i]}. Using this version"
  fi

  ## Download tools in tmpdir and install if newer version is available
  if [ "${CURRENT_TOOL_VERSION[$i]}" != "${AVAILABLE_TOOL_VERSION[$i]}" ]; then
    eval ${TOOLS_DOWNLOAD_CMD[$i]}
    RETVAL=$?
    if [ ${RETVAL} -eq 0 ]; then
      echo "Download of ${TOOLS[$i]} succesfull."
    else
     echo "Download of ${TOOLS[$i]} failed."
     exit ${RETVAL}
    fi
    debug

    install -t ${INSTALLDIR} ${TMPDIR}/${TOOLS[$i]}
    RETVAL=$?
    if [ ${RETVAL} -eq 0 ]; then
      echo "Succesfully installed ${TOOLS[$i]} ${AVAILABLE_TOOL_VERSION[$i]}."
    else
      echo "Installation of ${TOOLS[$i]} failed."
      exit ${RETVAL}
    fi
    rm ${TMPDIR}/${TOOLS[$i]}
  else
    echo "Version ${CURRENT_TOOL_VERSION[$i]} of ${TOOLS[$i]} is already right version."
  fi
done
debug

exit 0
