# scripts

- install_tools.sh
  
  Script to install precompiled binaries on a Linux x86_64 system:
  - kubectl
  - kustomize
  - flux
  - mc
  - k9s
  - yq
  - chainsaw
  - kyverno
